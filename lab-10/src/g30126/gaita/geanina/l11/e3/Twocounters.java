package g30126.gaita.geanina.l11.e3;
public class Twocounters extends Thread{
	int min,max;
	public Twocounters(String name) {
		super(name);
		this.min=0;
		this.max=100;
	}
	public Twocounters(String name,int min,int max) {
		super(name);
		this.min=min;
		this.max=max;
	}
	@Override
	public void run() {
		for(int i=min;i<max;i++)
		{
			System.out.println(getName() + " i = "+i);
            try {
                  Thread.sleep(100);
            } catch (InterruptedException e) {
                  e.printStackTrace();
            }
		}
		 System.out.println(getName() + " job finalised.");
	}
}
