package g30126.Gaita.Gianina.l2.e5;

import java.util.Random;
import java.util.Scanner;

public class RandomExamples {
	public static void main(String[] args) {
		Random r = new Random();
		Scanner in = new Scanner(System.in);
		System.out.println("Val lui N este ");
		int N = in.nextInt();
		int[] arr = new int[N];
		for (int i = 0; i < N; i++) {
			arr[i] = r.nextInt(100);
			System.out.println(arr[i] + " ");
		}
		bubbleSort(arr);
		System.out.println("");
		for (int i = 0; i < N; i++) {

			System.out.println(arr[i] + " ");
		}
	}

	public static void bubbleSort(int[] arr) {

		boolean swapped = true;

		int j = 0;

		int tmp;

		while (swapped) {

			swapped = false;

			j++;

			for (int i = 0; i < arr.length - j; i++) {

				if (arr[i] > arr[i + 1]) {

					tmp = arr[i];

					arr[i] = arr[i + 1];

					arr[i + 1] = tmp;

					swapped = true;

				}

			}

		}

	}
}
