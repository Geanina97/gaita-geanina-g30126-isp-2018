package gaita.geanina.l7.ex1;

import java.util.Comparator;

public class BaankAccount implements Comparable<BaankAccount>{
	private String owner;
	private double balance;

	public BaankAccount(String owner,double balance) {
		this.balance=balance;
		this.owner=owner;
	}
	public void withdraw(double amount)
	{
		if(balance<amount)
			System.out.println("nu e bani");
		else
			balance=balance-amount;
			
	}
	public void deposit(double amount) {
		balance=balance+amount;
		
	}
	public double getBalance() {
		return balance;
	}
	public String getOwner() {
		return owner;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof BaankAccount){
			BaankAccount bankAccount = (BaankAccount)obj;
			return owner == bankAccount.owner;
		}
		return false;
	}
	@Override
	public int hashCode() {
		  return (int) (owner.hashCode()+balance);
	}
	public static void main(String[] args) {
		BaankAccount bankAccount=new BaankAccount("lul", 10);
		BaankAccount bankAccount2=new BaankAccount("lulu", 50);
		if(bankAccount.equals(bankAccount2))
			System.out.println("are equal");
		else
			System.out.println("not equal");
		System.out.println(bankAccount.hashCode());
		bankAccount.deposit(50);
		System.out.println(bankAccount.getBalance());
	}
	@Override
	public int compareTo(BaankAccount o) {
		BaankAccount bankAccount=(BaankAccount) o;
		if(balance>bankAccount.balance)
			return 1;
		if(balance==bankAccount.balance)
			return 0;
		return -1;
	}
	public static Comparator<BaankAccount> bComparator=new Comparator<BaankAccount>() {
		@Override
		public int compare(BaankAccount b1,BaankAccount b2)
		{
			String owner1=b1.getOwner().toUpperCase();
			String owner2=b2.getOwner().toUpperCase();
			return owner1.compareTo(owner2);
			
		}
	};

}
