package gaita.geanina.l7.e2;

import java.util.*;

import gaita.geanina.l7.e1.BaankAccount;

public class Bank {
	ArrayList<BaankAccount> accounts= new ArrayList<BaankAccount>();

	public void addAccount(String owner, double balance) {
		BaankAccount b1= new BaankAccount(owner, balance);
		accounts.add(b1);
		//accounts.add(new BaankAccount(owner, balace));
		//accounts.add(new BaankAccount("Maraa", 22.1));
		//accounts.add(new BaankAccount("Ana", 111.1));
		//accounts.add(new BaankAccount("Ramona", 1000));
	}
	//o lista sortata dupa balance
	public void printAccounts() {
		Collections.sort(accounts, new Comparator<BaankAccount>() {

			@Override
			public int compare(BaankAccount b1, BaankAccount b2) {
				// TODO Auto-generated method stub
			if (b1.getBalance()>b2.getBalance()) { return 1;
			}
			else if (b1.getBalance()<b2.getBalance()) { return -1;
	}
			return 0;

}
		});
			for (BaankAccount account : accounts) {
				System.out.println("balance "+account.getBalance() +"owner "+account.getOwner());
				//System.out.println(account.getOwner());
			}
		}
	public void printAccounts(double minBalance, double maxBalance) {
		for (BaankAccount x : accounts) {
			if (minBalance < x.getBalance() && x.getBalance() < maxBalance) {
				System.out.println("balance " +x.getBalance() +"owner "+x.getOwner());
			}
		}
	}
	public void getAllAccounts() {
		Collections.sort(accounts, new Comparator<BaankAccount>() {

			@Override
			public int compare(BaankAccount b1, BaankAccount b2) {
				// TODO Auto-generated method stub
				return b1.getOwner().compareTo(b2.getOwner());
			}
			
		});
		for (BaankAccount x : accounts) {
				System.out.println("balance " +x.getBalance() +"owner "+x.getOwner());
		}
	}
	   public BaankAccount getAccount(String owner)
	    {
	        for(BaankAccount o:accounts){
	            if(owner.equals(o.getOwner()))
	                return o;
	        }
	        return null;
	    }
	public static void main(String[] args) {
		Bank bank= new Bank();
		bank.addAccount("mama", 122);
		bank.addAccount("ama", 20);
		bank.addAccount("cama", 1212);
		bank.addAccount("rama", 12);
		bank.printAccounts();
		System.out.println("----");
		bank.printAccounts(20,1220);
		System.out.println("---");
		bank.getAllAccounts();
		
    }
}
	



