package gaita.geanina.l7.e1;

//import java.util.Comparator;

//import javax.xml.parsers.DocumentBuilder;

public class BaankAccount  {
	private String owner;
	private double balance;
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	 public BaankAccount(String owner, double balance) {
		// TODO Auto-generated constructor stub
	this.balance=balance;
	this.owner=owner;
	}
	 public void withdraw(double amount){
			if(amount<=balance && amount>0) {
				balance=balance-amount;
			System.out.println("withdraw update for owner "+this.owner);
			}
			else {
				System.out.println("Nu mai sunt fonduri");
				
			}
		}
		public void deposit(double amount){
			balance=balance+amount;
			System.out.println("balance update for owner "+this.owner);
		}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(balance);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaankAccount other = (BaankAccount) obj;
		if (Double.doubleToLongBits(balance) != Double.doubleToLongBits(other.balance))
			return false;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner))
			return false;
		return true;
	}
	public static void main(String[] args) {
			BaankAccount b1= new BaankAccount("Ana", 22.1);
			BaankAccount b2= new BaankAccount("Cara", 20.1);
			BaankAccount b3= new BaankAccount("Cara", 20.1);
			BaankAccount b4= new BaankAccount("barca", 212.1);
			BaankAccount b5= new BaankAccount("dentist", 122.1);
System.out.println(b1.equals(b2));
System.out.println(b2.equals(b3));
System.out.println(b4.equals(b5));
System.out.println("check if withdraw and deposit work");
b2.deposit(400);
b1.withdraw(232);
}


	}
	


