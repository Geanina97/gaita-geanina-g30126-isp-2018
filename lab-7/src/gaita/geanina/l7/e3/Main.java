package gaita.geanina.l7.e3;


import java.util.TreeSet;

import gaita.geanina.l7.ex1.BaankAccount;


public class Main {

	public Main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		TreeSet<BaankAccount> bankAccounts=new TreeSet<BaankAccount>();
		Bank bank=new Bank(bankAccounts);
		bank.addAccounts("mara", 50.1);
		bank.addAccounts("isabela", 500);
		bank.addAccounts("acru", 1000);
		bank.addAccounts("clopotel", 5000);
		bank.printAccounts();
		bank.printAccount(40, 100);
		System.out.println(bank.getAllAccount());

	}
}
