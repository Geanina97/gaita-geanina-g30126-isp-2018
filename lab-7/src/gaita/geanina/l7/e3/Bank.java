package gaita.geanina.l7.e3;

import java.util.TreeSet;

import gaita.geanina.l7.ex1.BaankAccount;



public class Bank {
	private TreeSet<BaankAccount> bankAccount;

	public Bank(TreeSet<BaankAccount> bankAccounts) {
		this.bankAccount=bankAccounts;
	}
	
	public 	void addAccounts(String owner,double balance) {
		bankAccount.add(new BaankAccount(owner, balance));
	}
	public void printAccounts() {
		for(BaankAccount x:bankAccount)
		{		
			System.out.println(x.getOwner()+" "+x.getBalance());
		}
	}
	public void printAccount(double minBalance,double maxBalance) {
		for(BaankAccount x:bankAccount)
		{
			if(x.getBalance()>=minBalance && x.getBalance()<=maxBalance)
			{
				System.out.println(x.getOwner()+" "+x.getBalance());
			}
		}
	}
	public BaankAccount getAccounts(String owner)
	{
		for(BaankAccount x:bankAccount)
		{
			if(x.getOwner().equals(owner))
			{
				return x;
			}
		}
		return null;
	}
	public TreeSet<BaankAccount> getAllAccount()
	{
		return bankAccount;
	}

}
