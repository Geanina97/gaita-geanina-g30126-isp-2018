package g3012.gaita.geanina.l5.e3;

import java.util.Random;

public class TemperatureSensor extends Sensor{
    private Random rand = new Random();
    private int tempSensor = rand.nextInt(100);
    public int readValue()
    {
        return this.tempSensor;
    }
}