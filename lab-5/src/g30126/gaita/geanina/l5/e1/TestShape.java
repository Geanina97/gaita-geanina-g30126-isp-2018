package g30126.gaita.geanina.l5.e1;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestShape {
    @Test
    public void testGetAreaRect(){
        Shape rectangle = new Rectangle(2,4);
        rectangle.setColor("red");
        assertEquals(8, rectangle.getArea(),0.01);


    }

  
}
