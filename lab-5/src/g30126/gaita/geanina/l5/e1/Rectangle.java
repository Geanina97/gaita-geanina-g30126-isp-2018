package g30126.gaita.geanina.l5.e1;

public class Rectangle extends Shape {
	protected double width;
	protected double length;
	public Rectangle() {
	}
	public Rectangle(double width, double length){
		this.width=width;
		this.length=length;
	}
	public Rectangle(double width, double length, String color, boolean filled){
		super(color, filled);
		this.width=width;
		this.length=length;
		
	}
	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width=width;
	}
	public double getLength() {
		return length;
	}
	public void setLength(double length) {
		this.length=length;
	}
	public double getArea() {
		return getLength()*getWidth();
	}
   public double getPerimeter() {
	   return 2 * (getWidth()+getLength());
   }
   @Override
   public String toString() {
		return "Rectangle{" +
	               "color='" + color + '\'' +
	               ", filled=" + filled + 
	               '}';
	}
	   
   }
