package g30126.gaita.geanina.l5.e1;
public class Square extends Rectangle {
 public Square() {
	
 }
 public Square(double side) {
	 super.width= side;
     super.length= side;
 }
 public Square(double side, String color, boolean filled ) {
	 super(side, side,color, filled);
 }
 public double getSide() {
	 return getWidth();
 }
 public void setSide(double side) {
	 setWidth(side);
     setLength(side);
 }
 @Override
 public void setWidth(double side) {
     super.width=side;
     //super.setLength(side);
 }

 @Override
 public void setLength(double side) {
     //super.setWidth(side);
     super.length=side;
 }

 @Override
 public String toString() {
     return "Square{" +
             "+width=lenght="+width+
             "color='" + color + '\'' +
             ", filled=" + filled +
             '}';
}
}
