package g30126.gaita.geanina.l6.e5;
import java.awt.Color;
import java.awt.color.*;

import g30126.gaita.geanina.l6.e1r.DrawingBoard;
import g30126.gaita.geanina.l6.e1r.Rectangle;
import g30126.gaita.geanina.l6.e1r.Shape;
public class Pyramid extends Rectangle{
	
	public Pyramid(Color color, int length, int width) {
		super(color, length, width);
		// TODO Auto-generated constructor stub
	}
	private static final int nr = 5;
	private static final int width = 32;
	private static final int length = 15;
	
	public static void createPyramid() {
		  DrawingBoard b1 = new DrawingBoard();
		int i,j,c=0,k=nr;
		
		for(i=0;i<k;i++) {			
			for(j=i;j>0;j--) {
			Shape brick = new Rectangle(Color.RED, length, width);
				brick.setX(100+length*(j+1)+(k-i)*length/2);
				brick.setY(300-width*(k-i+1));
				brick.setfilled(false);
				brick.setId(" ");
				b1.addShape(brick);
				c++;
				
			}
		} 
	}
	public static void main(String[] args) {
		createPyramid();
	}
}