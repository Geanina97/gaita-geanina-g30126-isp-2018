package g30126.gaita.geanina.l9.e4;

public class Main {
	public Main() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws Exception{
		Car2 carFactory=new Car2();
		CAR car=carFactory.createCar("Dacia", 10000);
		CAR car2=carFactory.createCar("Volvo", 15000);
		carFactory.addCar(car, "car1.dat");
		carFactory.addCar(car2, "car2.dat");
		
		CAR car3=carFactory.removeCar("car1.dat");
		System.out.println(car3.toString());
		

	}

}

