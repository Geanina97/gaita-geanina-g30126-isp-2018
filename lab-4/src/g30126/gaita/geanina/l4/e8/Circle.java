package g30126.gaita.geanina.l4.e8;

public class Circle extends Shape {
	double radius;

	Circle() {
		super();
		radius = 1.0;
	}

	Circle(double radius) {
		super();
		this.radius = radius;
	}

	Circle(double radius, String color, boolean filled) {
		this.radius=radius;
		setColor(color);
		setFilled(filled);
	
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	public double getArea() {
		return Math.PI * radius * radius;
	}

	public double getPerimeter() {
		return 2 * Math.PI * radius;
	}

	@Override
	public String toString() {
		return "A Circle  with radius " + this.radius + " which is a subclass of " + super.toString();
	}

}
