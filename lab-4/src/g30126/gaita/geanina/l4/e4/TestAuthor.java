package g30126.gaita.geanina.l4.e4;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestAuthor {
	@Test
	public void test() {
		Author a1= new Author("Ana","ana_mara",'f');
		//System.out.println(a1.getEmail());
		//System.out.println(a1.toString());
		assertEquals(a1.toString(), "Ana(f) at ana_mara");
	}

}
