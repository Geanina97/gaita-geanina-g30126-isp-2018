package g30126.gaita.geanina.l4.e9;
import becker.robots.City;
import becker.robots.Direction;
import becker.robots.Robot;
import becker.robots.Thing;
import becker.robots.Wall;
	public class ex9 {

		 public static void main(String[] args)
		   {
		 City ny = new City();
	   Wall blockAve0 = new Wall(ny, 4, 1, Direction.SOUTH);
	   Wall blockAve2 = new Wall(ny, 4, 2, Direction.SOUTH);
	   Wall blockAve1 = new Wall(ny, 4, 1, Direction.WEST);
	   Wall blockAve3 = new Wall(ny, 2, 1, Direction.NORTH);
	   Wall blockAve4 = new Wall(ny, 2, 0, Direction.EAST);
	   Wall blockAve7 = new Wall(ny, 3, 0, Direction.EAST);
	   Wall blockAve6 = new Wall(ny, 4, 0, Direction.EAST);
	   Wall blockAve9 = new Wall(ny, 4, 2, Direction.EAST);
	   Robot karel = new Robot(ny, 1, 1, Direction.NORTH);

	   karel.move(); 
	   karel.turnLeft();
	   karel.turnLeft();    
	   karel.turnLeft();
	   karel.move();  
	  
	   karel.turnLeft();  
	   karel.turnLeft();  
	   karel.turnLeft();  
	   karel.move();
	   karel.move();
	   karel.move();
	   karel.move();
	   karel.turnLeft(); 
	   karel.turnLeft(); 
}
	}
