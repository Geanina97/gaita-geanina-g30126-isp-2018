package g30126.gaita.geanina.l4.e6;

import g30126.gaita.geanina.l4.e4.Author;

public class Book {
	private String name;
	private Author[] authors = new Author[5];
	private int numAuthors = 0;
	double price;
	int qtyInStock;
	
	public Book (String name, double price) {
		this.name = name;
		this.price = price;
	}
	public Book (String name, double price, int qtyInStock) {
		this.name = name;
		this.price = price;
		this.qtyInStock = qtyInStock;
	}
	public String getName(){
		return this.name;
	}
	
	public Author[] getAuthors() {
		return this.authors;
	}
	
	public double getPrice() {
		return this.price;
	}
	public void setPrice(double newPrice) {
		this.price = newPrice;
		return;
	}
	public int getQtyInStock() {
		return qtyInStock;
	}
	
	public void setQtyInStock(int qty) {
		this.qtyInStock = qty;
	}
	public String toString() {
		String strg = "'" + this.name + "' by " + authors.length + " author(s)";
		return strg;
	}
	public void comma() {
		System.out.print(", ");
	}
	public void printAuthors() {
		for(int i = 0;i<this.getAuthors().length - 1;++i) {
			if(this.getAuthors()[i] != null) {
				System.out.print(this.getAuthors()[i]);
				if(this.getAuthors()[i + 1] != null) {
					comma();
				}
			} else {
				System.out.print("");
			}
		}
		if(this.getAuthors()[this.getAuthors().length -1] != null) {
			System.out.print(this.getAuthors()[this.getAuthors().length -1]);
		} else {
			System.out.print("");
		}
	}
	
}

