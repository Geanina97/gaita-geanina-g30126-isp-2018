package g30126.gaita.geanina.l4.e6;

import g30126.gaita.geanina.l4.e4.Author;

public class TestBook{
	public static void main(String[] args) {
		Author[] authors = new Author[2];
		authors[0] = new Author("Tan Ah Teck", "AhTeck@somewhere.com", 'm');
		authors[1] = new Author("Paul Tan", "Paul@nowhere.com", 'm');
		Book javaDummy = new Book("Java for Dummy", 125.4, 1999);
		
		System.out.println(javaDummy);
		System.out.println(authors[0]);
		System.out.println(authors[1]);
	}
}