package g30126.gaita.geanina.l4.e5;

import g30126.gaita.geanina.l4.e4.Author;

public class Book {
private String name;
private double price;
private Author author;
private int qtyInStock=0;
public Book (String name, Author author, double price) {
	this.name=name;
	this.author=author;
	this.price=price;
}
public Book (String name, Author author, double price,
int qtyInStock) {
	this.qtyInStock=qtyInStock;
	this.name=name;
	this.author=author;
	this.price=price;
}
public String getName() {
	return name;
}
public Author getAuthor() {
	return author;
}
public double  getPrice() {
	return price;
			}
public void setPrice(double price ) {
	this.price=price;
}
public int getQtyInStock() {
	return qtyInStock;
}
public void  setQtyInStock(int qtyInStock ) {
	this.qtyInStock=qtyInStock;
}
public String toString() {
	return this.name +" by "+this.author.toString();
}
}
