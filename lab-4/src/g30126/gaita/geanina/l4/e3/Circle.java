package g30126.gaita.geanina.l4.e3;

public class Circle {
private double radius;
private String color;
public Circle() {
	radius = 1.0;
    color = "red";
}
public Circle(double radius) {
	this.radius=radius;
}
public double getArea() {
	return Math.PI*radius*radius;
}
public double getRadius() {
	return radius;
}
public static void main(String[] args)
{
Circle c1 = new Circle();
System.out.println(c1.getArea());
System.out.println(c1.getRadius());
}
}
