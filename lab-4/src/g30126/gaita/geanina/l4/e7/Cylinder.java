package g30126.gaita.geanina.l4.e7;

import g30126.gaita.geanina.l4.e3.Circle;

public class Cylinder extends Circle {
private	double height;
	Cylinder(){
		super();
		height=1.0;
			  }
	Cylinder(double radius){
		super(radius);
	}
	Cylinder(double radius, double height){
		super(radius);
		this.height=height;
	}
	public double getHeight() {
		return height;
	}
public double getVolume() {
	return getArea()*height;
}
}
